import React from "react";
import Router from "next/router";
const Users = ({ user }) => {
  return (
    <li
      className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
      onClick={(e) => Router.push("/users/[id]", `/users/${user.id}`)}
    >
      <div>
        <h5>
          {user.first_name} {user.last_name}
        </h5>
        <p>{user.email}</p>
      </div>
      <img
        src={user.avatar}
        style={{ borderRadius: "50%" }}
        alt={user.first_name + user.last_name}
      />
    </li>
  );
};

export default Users;
