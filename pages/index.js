import React from "react";
import Container from "../components/Container";
import Users from "../components/Users";
import fetch from "isomorphic-fetch";
const Index = ({ users }) => {
  return (
    <Container titulo="Home">
      <h1>Next</h1>
      <ul className="list-group">
        {users.map((user) => (
          <Users user={user} key={user.id} />
        ))}
      </ul>
    </Container>
  );
};

Index.getInitialProps = async () => {
  const res = await fetch("https://reqres.in/api/users");
  const resJSON = await res.json();
  return { users: resJSON.data };
};

export default Index;
